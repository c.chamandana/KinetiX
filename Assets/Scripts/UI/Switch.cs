﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[ExecuteInEditMode]
public class Switch : MonoBehaviour
{
    public enum Selection
    {
        Left, Right
    }

    public Selection Selected = Selection.Left;

    private Image LeftSwitch;
    private Image RightSwitch;
    private Image LeftSwitchIcon;
    private Image RightSwitchIcon;

    public UnityEvent OnSwitch;

    void Awake()
    {
        // Instantiate the two sides
        LeftSwitch = transform.GetChild(0).gameObject.GetComponent<Image>();
        RightSwitch = transform.GetChild(1).gameObject.GetComponent<Image>();
        // Get the switch icons the component tree
        LeftSwitchIcon = LeftSwitch.transform.GetChild(0).gameObject.GetComponent<Image>();
        RightSwitchIcon = RightSwitch.transform.GetChild(0).gameObject.GetComponent<Image>();
    }

    void Start()
    {
        _UpdateGraphics();
    }

    /// <summary>
    /// Sets the current selection of the switch
    /// </summary>
    /// <param name="side"></param>
    public void Set (string side)
    {
        Selected = (side == "Left") ? Selection.Left : Selection.Right;
        _UpdateGraphics();
        OnSwitch.Invoke();
    }
    
    Color bgDeselect = new Color(1, 1, 1, 18f / 255f);
    Color bgSelectIcon = new Color(0, 0, 0, 153f / 255f);
    Color bgDeselectIcon = new Color(1, 1, 1, 50f / 255f);

    private void _UpdateGraphics()
    {
        // Get the selection switch
        Image selectedSwitch = null;
        Image selectedSwtichIcon = null;

        switch (Selected)
        {
            case Selection.Left:
                selectedSwitch = LeftSwitch;
                selectedSwtichIcon = LeftSwitchIcon;
                break;
            case Selection.Right:
                selectedSwitch = RightSwitch;
                selectedSwtichIcon = RightSwitchIcon;
                break;
        }

        // Deselect all the switches
        LeftSwitch.color = bgDeselect;
        RightSwitch.color = bgDeselect;
        LeftSwitchIcon.color = bgDeselectIcon;
        RightSwitchIcon.color = bgDeselectIcon;

        // Select the selected switch side only
        selectedSwitch.color = Color.white;
        selectedSwtichIcon.color = bgSelectIcon;    
    }
}
