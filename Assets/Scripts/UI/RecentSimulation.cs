﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecentSimulation : MonoBehaviour
{
    public string SimulationName;
    public string SimulationID;

    private Text _simName;
    private Text _simID;

    void Start()
    {
        _simName = transform.GetChild(0).gameObject.GetComponent<Text>();
        _simID = transform.GetChild(1).gameObject.GetComponent<Text>();

        _simName.text = SimulationName;
        _simID.text = "http://sim.kinetix.com/" + SimulationID;
    }
}
