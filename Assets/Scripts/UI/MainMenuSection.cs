﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuSection : MonoBehaviour
{
    private CanvasGroup cg;

    void Awake()
    {
        cg = GetComponent<CanvasGroup>();
    }

    void OnEnable()
    {
        cg.alpha = 0;
        // Start the fading in effect
        StartCoroutine(StartFadingIn());
    }

    public void Close()
    {
        StartCoroutine(StartFadingOut());
    }

    /// <summary>
    /// Fades in a section
    /// </summary
    IEnumerator StartFadingIn()
    {
        while (cg.alpha < 1)
        {
            cg.alpha += Time.deltaTime * 5f;
            yield return null;
        }
    }

    /// <summary>
    /// Fades in a section
    /// </summary
    IEnumerator StartFadingOut()
    {
        while (cg.alpha > 0)
        {
            cg.alpha -= Time.deltaTime * 5f;
            yield return null;
        }

        gameObject.SetActive(false);
    }
}
