﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Forces : MonoBehaviour
{
    [Header("Prefab Connections")]
    public GameObject ForceContainer;

    [Header("GameObject Connections")]
    public Transform _ForcesList;
    public Selector _Selector;

    [Header("Properties")]
    public GameObject Selected;

    void Awake()
    {
        Global._ForcesPanel = this;
    }

    /// <summary>
    /// Creates a new Force UI Element based on the given data, PS: All the forces are acting on the center of mass of all the objects in the KEditor
    /// </summary>
    /// <param name="Magnitude">The magnitude of the force vector</param>
    /// <param name="Angle">The angle of the force vector</param>
    public void AddNewForce(float Magnitude = 1, float Angle = 90)
    {
        // Create a new Force UI Object
        var container = Instantiate(ForceContainer, _ForcesList).transform;
        container.GetChild(1).GetComponent<InputField>().text = Magnitude.ToString(); // Set the magnitude
        container.GetChild(3).GetComponent<InputField>().text = Angle.ToString(); // Set the angle

        // Update the angle showcase of the contianer we just created
        container.gameObject.GetComponent<ForceContainer>().UpdateAngleShowcase();
    }

    public void AddNewForceButton()
    {
        if (_Selector._Selection != null)
        {
            AddNewForce();
            // Apply the forces
            SetForcesToObject();
        }
    }

    /// <summary>
    /// Set the elements in the Forces UI to the object itself
    /// </summary>
    public void SetForcesToObject()
    {
        // Set the forces to the selected object
        var kobject = _Selector._Selection.GetComponent<KObject>();

        // Get all the children gameObject
        var newForces = new List<Force>();

        foreach (Transform child in _ForcesList)
        {
            newForces.Add(new Force()
            {
                Magnitude = float.Parse(child.GetChild(1).GetComponent<InputField>().text),
                Angle = float.Parse(child.GetChild(3).GetComponent<InputField>().text)
            });
        }

        kobject._Forces = newForces;

        // Recalculate the forces
        Global._Selector.AddObjectTrajectory();

        // Save the file
        _Selector.gameObject.GetComponent<KEditor.Simulation>().CreateSimulationObjectFromScene();
    }

    /// <summary>
    /// Clears all the forces objects in the Forces UI
    /// </summary>
    public void Clear()
    {
        foreach (Transform child in _ForcesList)
        {
            Destroy(child.gameObject);
        }
    }
}
