﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ForceContainer : MonoBehaviour
{
    public InputField Angle;
    public RectTransform _AngleTick;

    /// <summary>
    /// Updates the angle showcase circle in the left side
    /// </summary>
    public void UpdateAngleShowcase()
    {
        try
        {
            // Parse the angle to degrees
            var angle = float.Parse(Angle.text);
            // Rotate the angle tick
            _AngleTick.eulerAngles = new Vector3(0, 0, angle);
        }
        catch (Exception)
        {
            Angle.text = "90";
        }
    }

    /// <summary>
    /// Removes the UI attachment of the force acting on the selected gameObject
    /// </summary>
    public void RemoveMyself()
    {
        DestroyImmediate(gameObject);
    }
}