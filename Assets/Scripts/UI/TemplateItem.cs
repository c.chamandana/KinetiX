﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TemplateItem : MonoBehaviour
{
    public Sprite Thumbnail;
    public string _TemplateName;

    private Image TemplateThumbnail;
    private Text TemplateName;

    void Start()
    {
        TemplateThumbnail = transform.GetChild(0).gameObject.GetComponent<Image>();
        TemplateName = transform.GetChild(1).gameObject.GetComponent<Text>();

        // Set the template values
        TemplateThumbnail.sprite = Thumbnail;
        TemplateName.text = _TemplateName;
    }
}
