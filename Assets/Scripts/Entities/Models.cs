﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// A Simulation document is represented by this class
/// </summary>
[Serializable]
public class Simulation
{
    public string name;
    public string id;
    public float gravity;
    public bool gravityEnabled;
    public string localFile;
    public List<Element> elements;

    public Simulation(string _name, string _id, List<Element> _elements, float _gravity, bool _gravityEnabled, string _localFile)
    {
        name = _name;
        id = _id;
        elements = _elements;
        gravity = _gravity;
        gravityEnabled = _gravityEnabled;
        localFile = _localFile;
    }
}

/// <summary>
/// Each object in the current simulation
/// </summary>
[Serializable]
public class Element
{
    public string name;
    public string type;
    public Vector position;
    public Vector rotation;
    public float mass;
    public int material;
    public int collisions;
    public List<Force> forces;

    public Element(string _name, string _type, Vector _position, Vector _rotation, float _mass, int _material, int _collisions, List<Force> _forces)
    {
        name = _name;
        type = _type;
        position = _position;
        rotation = _rotation;
        mass = _mass;
        material = _material;
        collisions = _collisions;
        forces = _forces;
    }
}

[Serializable]
public class Vector
{
    public float X;
    public float Y;
    public float Z;

    public Vector(float _x, float _y, float _z)
    {
        X = _x;
        Y = _y;
        Z = _z;
    }
}

/// <summary>
/// Represents a single force acting on a single object in the KinetiX Editor.
/// </summary>
[Serializable]
public class Force
{
    /// <summary>
    /// Angle in degrees between 0 - 360 (anti-clockwise)
    /// </summary>
    public float Angle;
    /// <summary>
    /// The Force Magnitude in Newtons
    /// </summary>
    public float Magnitude;
}