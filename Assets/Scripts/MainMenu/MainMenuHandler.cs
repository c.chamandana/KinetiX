﻿using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// TODO : IMPLEMENT THESE STUFF
public class MainMenuHandler : MonoBehaviour
{
    [Header("GameObject Connections")]
    public GameObject NewSimulation;
    public GameObject LoadSimulation;
    public GameObject Templates;
    public GameObject RecentSimulationsContainer;
    public GameObject SimulationItem;
    public GameObject Preloader;
    public InputField SimulationNameInput;
    public InputField SimulationOpenLinkInput;

    /// <summary>
    /// Opens the new simulation create panel
    /// </summary>
    public void New()
    {
        // Show the preloader
        Preloader.SetActive(true);

        // Get the database reference
        var id = new System.Random().Next(1000000, 9000000);

        // Create the simulation object
        Global.SimulationObject = new Simulation("Untitled Simulation", id.ToString(), new List<Element>(), -9.80665f, true, id.ToString() + ".sim");

        // Set the sim link
        SimulationNameInput.text = "Untitled Simulation";
        SimulationOpenLinkInput.text = "http://sim.kinetix.com/" + id.ToString();

        // Hide the preloader
        Preloader.SetActive(false);
        // Show the new simulation form
        NewSimulation.SetActive(true);
    }

    /// <summary>
    /// Creates a new simulations and opens it in the editor
    /// </summary>
    public void CreateNewSimulation()
    {
        // Get the name form the input field
        var simulationName = SimulationNameInput.text;

        // Create the global simulation object
        Global.SimulationObject = new Simulation(simulationName, Global.SimulationObject.id, new List<Element>(), Global.SimulationObject.gravity, true, Global.SimulationObject.localFile);

        // Show the preloader
        Preloader.SetActive(true);

#if (UNITY_ANDROID)
        // Get the database reference for the simulation
        var dbref = Global._Firebase.Child("sims").Child(Global.SimulationObject.id.ToString());
        dbref.SetRawJsonValueAsync(JsonUtility.ToJson(Global.SimulationObject)).ContinueWith((t) =>
        {
            // Add the Simulation to the recent simulations list to be opened later
            Recents.AddRecent(Global.SimulationObject);

            // Load the editor
            SceneManager.LoadSceneAsync("Editor");
        });
#else
        // Add the Simulation to the recent simulations list to be opened later
        Recents.AddRecent(Global.SimulationObject);

        // Load the editor
        SceneManager.LoadSceneAsync("Editor");
#endif
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            GameObject toClose = null;

            if (NewSimulation.activeInHierarchy) toClose = NewSimulation;
            if (LoadSimulation.activeInHierarchy) toClose = LoadSimulation;
            if (Templates.activeInHierarchy) toClose = Templates;

            toClose.GetComponent<MainMenuSection>().Close();
        }
    }

    void Start()
    {
#if (UNITY_ANDROID)
        // Initialize android Toasts
        Toast.Init();

        // Resolve the Firebase Dependencies (Google Play Services)
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://ninponix-kinetix.firebaseio.com/");
                Global._Firebase = FirebaseDatabase.DefaultInstance.RootReference;
            }
            else
            {
                Toast.ShowToastOnUiThread("Google Play Services not found");
                Debug.LogError(System.String.Format(
                  "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
            }
        });
#endif
    }

    /// <summary>
    /// Opens the load simulation panel
    /// </summary>
    public void OpenLoadPanel()
    {
        LoadSimulation.SetActive(true);

        // Render the recently opened simulations
        GetRecentSims();
    }

    /// <summary>
    /// Loads the recents files and displays them in the list
    /// </summary>
    public void GetRecentSims()
    {
        // Clear the previous recents
        foreach (Transform child in RecentSimulationsContainer.transform)
        {
            Destroy(child.gameObject);
        }

        // Get the recent files
        var recents = Recents.GetRecents();

        // Create the ui threads
        foreach (var recent in recents)
        {
            var sim = Instantiate(SimulationItem, RecentSimulationsContainer.transform);
            var r = sim.GetComponent<RecentSimulation>();

            r.SimulationName = recent.SimulationName;
            r.SimulationID = recent.SimulationID;

            sim.GetComponent<Button>().onClick.AddListener(() => 
            {
                LoadSimulationFromID(recent.SimulationID);
            });
        }   
    }

    /// <summary>
    /// Loads a certain simulation from the servers based on its Simulation ID
    /// </summary>
    /// <param name="ID">ID of the simulation</param>
    public void LoadSimulationFromID(string ID)
    {
        // TODO: DOWNLOAD THE SIMULATION AND CONTINUE
        Debug.Log("Loading Simulation : " + ID);

        Preloader.SetActive(true);

        Global._Firebase.Child("sims").Child(ID).GetValueAsync().ContinueWith((t) =>
        {
            if (t.Result.Value != null)
            {
                // The simulation has been found on the servers.
                Debug.Log(t.Result.GetRawJsonValue());
                Global.SimulationObject = Newtonsoft.Json.JsonConvert.DeserializeObject<Simulation>(t.Result.GetRawJsonValue());
                //Global.SimulationObject = JsonUtility.FromJson<Simulation>(t.Result.GetRawJsonValue());
                SceneManager.LoadSceneAsync("Editor"); // Load the editor fast

                Preloader.SetActive(false);
            }
            else
            {
                // The simulation wasn't found in the servers
                Toast.ShowToastOnUiThread("Could not load simulation from the servers");
                
                Preloader.SetActive(false);
            }
        });
    }

    /// <summary>
    /// Opens the library on a browser
    /// </summary>
    public void OpenLibrary()
    {

    }

    /// <summary>
    /// Opens the templates window
    /// </summary>
    public void OpenTemplatesWindow()
    {
        Templates.SetActive(true);   
    }

    public void CreateProjectFromTemplate(string templateJSON)
    {

    }

    /// <summary>
    /// Opens the open learning window
    /// </summary>
    public void OpenLearning()
    {
        Preloader.SetActive(true);
        SceneManager.LoadSceneAsync("Learn");
    }

    /// <summary>
    /// Open the physics calculator
    /// </summary>
    public void OpenCalculator()
    {
        Preloader.SetActive(true);
        SceneManager.LoadSceneAsync("Calculator");
    }

    /// <summary>
    /// Opens the settings window
    /// </summary>
    public void OpenSettings()
    {
        Preloader.SetActive(true);
        SceneManager.LoadSceneAsync("Settings");
    }

    /// <summary>
    /// Opens help
    /// </summary>
    public void OpenHelp()
    {
        Preloader.SetActive(true);
        Application.OpenURL("http://ninponix-kinetix.firebaseapp.com/help");
        Preloader.SetActive(false);
    }
}
