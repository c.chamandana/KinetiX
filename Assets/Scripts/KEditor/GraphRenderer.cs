﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;
using UnityEngine.UI.Extensions;

public class GraphRenderer : MonoBehaviour
{
    [Header("GameObject Connections")]
    public GameObject GraphView;
    public Text MinimumY;
    public Text MaximumY;
    public Text MaximumT;
    public GameObject DisplacementGraph;
    public GameObject VelocityGraph;
    public UILineRenderer DisplacementX;
    public UILineRenderer DisplacementY;
    public UILineRenderer VelocityX;
    public UILineRenderer VelocityY;

    public enum GraphType
    {
        VT, DT
    }

    float simTimer = 0f;
    bool graphingStarted = false;

    public void ShowGraph()
    {
        if (Global._Selector._Selection != null)
        {
            // If the simulation has started
            if (Global.SimulationStarted)
            {
                // Reset the simulation timer
                simTimer = 0f;

                // Show the graph view
                GraphView.SetActive(true);
                Global.GraphViewEnabled = true;

                graphingStarted = true;
                Displacement.Clear();
                Velocity.Clear();

                // Rendering the Displacement-time graph for the motion of the selected object
                StartCoroutine(EnqueueGraphs());
                StartCoroutine(Graph());
            }
            else
            {
                // Run the simulation and try again
                Toast.ShowToastOnUiThread("Starting simulation");
                GetComponent<KEditor.Simulation>().Run();
                ShowGraph();
            }
        }
        else
        {
            Toast.ShowToastOnUiThread("Please select an object first");
        }
    }
    
    /// <summary>
    /// Switch between DT and VT Graphs
    /// </summary>
    public void SwitchGraph()
    {
        CurrentGraphType = (CurrentGraphType == GraphType.DT) ? GraphType.VT : GraphType.DT;

        // Switch view based on graph type
        switch (CurrentGraphType)
        {
            case GraphType.DT:
                DisplacementGraph.SetActive(true);
                VelocityGraph.SetActive(false);
                break;
            case GraphType.VT:
                DisplacementGraph.SetActive(false);
                VelocityGraph.SetActive(true);
                break;
        }
    }

    public void HideGraph()
    {
        GraphView.SetActive(false);
        graphingStarted = false;
    }

    public GraphType CurrentGraphType = GraphType.DT;
    public List<Vector2> Displacement = new List<Vector2>();
    public List<Vector2> Velocity = new List<Vector2>();

    void Update()
    {
        if (graphingStarted)
        {
            // calculate the simulation time
            simTimer += Time.deltaTime;

            if (simTimer > 5f)
            {
                // Stop the simulation
                GetComponent<KEditor.Simulation>().Stop();
                graphingStarted = false;
            }
        }
    }

    IEnumerator EnqueueGraphs()
    {
        while (graphingStarted)
        {
            Displacement.Add(Global._Selector._Selection.transform.position);
            Velocity.Add(Global._Selector._selectionrb.velocity);

            yield return new WaitForSeconds(0.1f);
        }
    }

    IEnumerator Graph()
    {
        // Update the graph every 1 second
        while (graphingStarted)
        {
            if (CurrentGraphType == GraphType.DT)
            {
                // Get the highest value on the DT Values
                var max = Displacement.Max((x) =>
                {
                    if (x.x > x.y)
                        return x.x;
                    else
                        return x.y;
                });
                var min = Displacement.Min((x) =>
                {
                    if (x.x < x.y)
                        return x.x;
                    else
                        return x.y;
                });

                // Show the minimum, maximum and time
                MaximumY.text = max.ToString();
                MinimumY.text = min.ToString();
                MaximumT.text = Math.Round(simTimer, 2).ToString();

                var diff = Math.Abs(max - min);

                // Calculate the normalized graph values
                List<Vector2> calcx = new List<Vector2>();
                List<Vector2> calcy = new List<Vector2>();

                var heightPerVal = (Screen.height - 225f) / diff;
                var widthPerVal = (Screen.width - 110f) / simTimer;

                for (var i = 0; i < Displacement.Count; i++)
                {
                    var d = Displacement[i];
                    calcx.Add(new Vector2(0.1f * i * widthPerVal, d.x * heightPerVal + min));
                    calcy.Add(new Vector2(0.1f * i * widthPerVal, d.y * heightPerVal + min));
                }

                DisplacementX.Points = calcx.ToArray();
                DisplacementY.Points = calcy.ToArray();
            }
            else
            {
                // Get the highest value on the DT Values
                var max = Velocity.Max((x) =>
                {
                    if (x.x > x.y)
                        return x.x;
                    else
                        return x.y;
                });
                var min = Velocity.Min((x) =>
                {
                    if (x.x < x.y)
                        return x.x;
                    else
                        return x.y;
                });

                // Show the minimum, maximum and time
                MaximumY.text = max.ToString();
                MinimumY.text = min.ToString();
                MaximumT.text = Math.Round(simTimer, 2).ToString();

                var diff = Math.Abs(max - min);

                // Calculate the normalized graph values
                List<Vector2> calcx = new List<Vector2>();
                List<Vector2> calcy = new List<Vector2>();

                var heightPerVal = (Screen.height - 225f) / diff;
                var widthPerVal = (Screen.width - 110f) / simTimer;

                for (var i = 0; i < Velocity.Count; i++)
                {
                    var d = Velocity[i];
                    calcx.Add(new Vector2(0.1f * i * widthPerVal, d.x * heightPerVal + Mathf.Abs(min)));
                    calcy.Add(new Vector2(0.1f * i * widthPerVal, -d.y * heightPerVal + Mathf.Abs(min)));
                }

                VelocityX.Points = calcx.ToArray();
                VelocityY.Points = calcy.ToArray();
            }

            yield return new WaitForSeconds(0.1f);
        }
    }
}
