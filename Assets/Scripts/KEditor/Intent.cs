﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Intent : MonoBehaviour
{ 
    void Start()
    {
        try
        {
            // Initialize the toast
            Toast.Init();
            AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaObject intent = currentActivity.Call<AndroidJavaObject>("getIntent");
            var arguments = intent.Call<string>("getDataString");

            if (arguments != null && arguments != "")
            {
                // User has opened a link
                var handle = arguments.Split('/').Last();

                GetComponent<MainMenuHandler>().LoadSimulationFromID(handle);
            }
        }
        catch (Exception) {

        }
    }

    void Update()
    {

    }
}
