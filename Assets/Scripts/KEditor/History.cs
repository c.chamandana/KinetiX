﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class History
{
    /// <summary>
    /// The list of simulations (history of simulations)
    /// </summary>
    public static Stack<Simulation> _History = new Stack<Simulation>();

    /// <summary>
    /// Current simulation component the editor is running right now
    /// </summary>
    public static KEditor.Simulation _CurrentSimulation;


    public static void Undo()
    {
        // Time travel
        _CurrentSimulation._CurrentSimulation = _History.Pop();
        // Render the previous history object
        _CurrentSimulation.CreateSceneFromSimulationObject();
    }

}