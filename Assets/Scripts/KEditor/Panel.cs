﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Panel : MonoBehaviour {

    public enum PanelState { Collapsed, Expanded };
    public enum Direction { Horizontal, Vertical };

    [Header("Status of the Panel")]
    public PanelState _State = PanelState.Collapsed;
    public Direction _Direction = Direction.Horizontal;
    public float _ExpandedWidth = 250f;
}
