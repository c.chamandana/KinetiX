﻿using UnityEngine;

public static class Toast
{
    static string toastString;
    static string input;
    static AndroidJavaObject currentActivity;
    static AndroidJavaClass UnityPlayer;
    static AndroidJavaObject context;
    
    public static void Init()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
            context = currentActivity.Call<AndroidJavaObject>("getApplicationContext");
        }
    }

    public static void ShowToastOnUiThread(string t)
    {
#if (UNITY_ANDROID && !UNITY_EDITOR)
        toastString = t;
        currentActivity.Call("runOnUiThread", new AndroidJavaRunnable(ShowToast));
#else
        Debug.Log(t);
#endif
    }

    static void ShowToast()
    {
        AndroidJavaClass Toast = new AndroidJavaClass("android.widget.Toast");
        AndroidJavaObject javaString = new AndroidJavaObject("java.lang.String", toastString);
        AndroidJavaObject toast = Toast.CallStatic<AndroidJavaObject>("makeText", context, javaString, Toast.GetStatic<int>("LENGTH_SHORT"));
        toast.Call("show");
    }
}