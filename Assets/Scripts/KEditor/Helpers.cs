﻿using System;
using System.Collections;
using UnityEngine;

/// <summary>
/// Contains helper functions for the entire editor to use
/// </summary>
public class Helpers : MonoBehaviour
{
    public GameObject Graphing;
    Toggler toggler;

    void Start()
    {
        Global._Helpers = this;
        toggler = GetComponent<Toggler>();
    }

    /// <summary>
    /// Check if the retrieved position is inside the editor or one of it's panels
    /// </summary>
    /// <param name="pos">Input position to check</param>
    /// <returns></returns>
    public bool CheckBounds(Vector3 pos)
    {
        var objects = toggler.Objects.gameObject.GetComponent<Panel>();
        var properties = toggler.Properties.gameObject.GetComponent<Panel>();
        var forces = toggler.Forces.gameObject.GetComponent<Panel>();
        var simulation = toggler.Simulation.gameObject.GetComponent<Panel>();

        return
        !(
            ((objects._State == Panel.PanelState.Expanded && pos.x < objects._ExpandedWidth) || (objects._State == Panel.PanelState.Collapsed && pos.x < 50)) ||
            ((properties._State == Panel.PanelState.Expanded && pos.x < properties._ExpandedWidth) || (properties._State == Panel.PanelState.Collapsed && pos.x < 50)) ||
            ((forces._State == Panel.PanelState.Expanded && pos.x < forces._ExpandedWidth) || (forces._State == Panel.PanelState.Collapsed && pos.x < 50))
            || (Graphing.activeInHierarchy)
            //((simulation._State == Panel.PanelState.Expanded && pos.y < simulation._ExpandedWidth) || (simulation._State == Panel.PanelState.Collapsed && pos.y < 50))
        );
    }

    /// <summary>
    /// Get the index of the specified material type on the dropdown
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public int matResolve(KObject.Material type)
    {
        var mat = 0;

        switch (type)
        {
            case KObject.Material.None:
                mat = 0;
                break;
            case KObject.Material.FullFriction:
                mat = 1;
                break;
            case KObject.Material.Bouncy:
                mat = 2;
                break;
            case KObject.Material.Bouncy_Friction:
                mat = 3;
                break;
        }

        return mat;
    }

    public KObject.Material matResolveReverse(int index)
    {
        switch (index)
        {
            case 0:
                return KObject.Material.None;
            case 1:
                return KObject.Material.FullFriction;
            case 2:
                return KObject.Material.Bouncy;
            case 3:
                return KObject.Material.Bouncy_Friction;
            default:
                return KObject.Material.None;
        }
    }

}
