﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Selector : MonoBehaviour
{
    /// <summary>
    /// Selection in the editor
    /// </summary>
    public GameObject _Selection = null;

    [Header("GameObject connections")]
    public GameObject PropertyEmptyMessage;
    public GameObject ForceEmptyMessage;
    public Forces ForcesUI;
    public LineRenderer TrajectoryRenderer;
    public GameObject TouchdownObject;

    [Header("Properties Panel")]
    public InputField Name;
    public InputField PosX;
    public InputField PosY;
    public InputField PosZ;
    public InputField RotX;
    public InputField RotY;
    public InputField RotZ;
    public InputField Mass;
    public Dropdown Material;
    public Dropdown Collision;

    [Header("Floaters")]
    public RectTransform ObjectFloater;
    public Text objName;

    private Vector2 _objectFloaterNewPosition;
    private Vector2 _mouseDownPosition;
    private KEditor.Simulation _simulation;
    public Rigidbody _selectionrb = null;

    void Start()
    {
        _simulation = GetComponent<KEditor.Simulation>();
        Global._Selector = this;
        PropertyEmptyMessage.SetActive(true);
        ObjectFloater.gameObject.SetActive(false);
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
            _mouseDownPosition = Input.mousePosition;

        if (Input.GetMouseButtonUp(0))
        {
            var diff = (Vector3.Distance(Input.mousePosition, _mouseDownPosition));
            
            if (diff < (Screen.width / 50))
            {
                if (Global._Helpers.CheckBounds(Input.mousePosition))
                    Selection(Input.mousePosition);
            }
        }

        // Update the selection properties
        UpdateProps();
        // Update the positions of the selected objects
        UpdateSelectionTranslation();

        if (Global.SimulationStarted)
        {
            if (_Selection != null)
            {
                ObjectFloater.gameObject.SetActive(false);
                TrajectoryRenderer.gameObject.SetActive(false);
                TouchdownObject.SetActive(false);
            }
        }
        else
        {
            if (_selectionrb != null)
            {
                if (_selectionrb.useGravity)
                {
                    // Hide the trajectory Render if the selection is null
                    TrajectoryRenderer.gameObject.SetActive(_Selection != null);
                    TouchdownObject.SetActive(_Selection != null);

                    if (_Selection == null)
                    {
                        ObjectFloater.gameObject.SetActive(false);
                    }
                }
                else
                {
                    TrajectoryRenderer.gameObject.SetActive(false);
                    TouchdownObject.SetActive(false);
                }
            }
            else
            {
                // Hide the trajectory Render if the selection is null
                TrajectoryRenderer.gameObject.SetActive(_Selection != null);
                TouchdownObject.SetActive(_Selection != null);

                if (_Selection == null)
                {
                    ObjectFloater.gameObject.SetActive(false);
                }
            }
        }

        // Animate the trajectory
        AnimateTrajectoryTexture();

        if (_Selection != null)
        {
            var pos = Camera.main.WorldToScreenPoint(new Vector3(_Selection.transform.position.x, _Selection.transform.position.y, _Selection.transform.position.z));
            ObjectFloater.anchoredPosition = new Vector2(pos.x, pos.y - 100);
        }
    }

    /// <summary>
    /// Animates the trajectory offset
    /// </summary>
    void AnimateTrajectoryTexture()
    {
        TrajectoryRenderer.material.mainTextureOffset = new Vector2(TrajectoryRenderer.material.mainTextureOffset.x - Time.deltaTime / 2, 1);
    }

    void UpdateFloaterPosition()
    {
        if (_Selection != null)
        {
            var screenPos = Camera.main.WorldToScreenPoint(_Selection.transform.position);
            _objectFloaterNewPosition = new Vector2(screenPos.x, screenPos.y - 105);

            objName.text = _Selection.name;
        }
    }

    public Vector3 _selectionOriginalPosition;

    void Selection(Vector3 mousePos)
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(mousePos);

        if (Physics.Raycast(ray, out hit, 50f))
        {
            if (hit.collider.gameObject != null)
            {   
                // Select and silhoette the object
                Select(hit.collider.gameObject);
                Silhouette(hit.collider.gameObject);
            }
        }
    }

    public void Deselect()
    {
        _Selection = null;
        // Clear the Forces UI
        ForcesUI.Clear();
        // Show the forces empty message
        ForceEmptyMessage.SetActive(true);
    }

    void Silhouette(GameObject obj)
    {
        /*
         * GOING WRONG WITH UNITY_ANDROID so... NO!
         * 
            // Disable all the silhouettes
            var objects = GameObject.FindGameObjectsWithTag("Objects");
            foreach (var i in objects)
            {
                i.GetComponent<Renderer>().material.SetFloat("_Outline", 0);
            }

            // Set the silhuoette for the selected item only
            obj.GetComponent<Renderer>().material.SetFloat("_Outline", obj.transform.localScale.x * 0.05f);
        */
    }

    public void Select(GameObject obj)
    {
        _Selection = obj;
        // Hide the empty message
        PropertyEmptyMessage.SetActive(false);
        // Hide the forces empty message
        ForceEmptyMessage.SetActive(false);
        // Update the selection properties
        SelectionPropeties();
        // Show the Object Floater
        ObjectFloater.gameObject.SetActive(true);
        // Animate the Object Floater to the target position
        UpdateFloaterPosition();

        _selectionrb = obj.GetComponent<Rigidbody>();

        _selectionOriginalPosition = obj.transform.position;

        if (_selectionrb.useGravity)
        {
            // Update the Forces UI with the forces attached to it
            UpdateForcesUI();
            // Update the trajectory fo the object
            AddObjectTrajectory();
        }
        else
        {
            // Hide the forces and the trajectory
            ForceEmptyMessage.SetActive(true);
        }
    }

    /// <summary>
    /// Update the Forces UI for the selected Object
    /// </summary>
    private void UpdateForcesUI()
    {
        // Remove the force Objects in the ui
        ForcesUI.Clear();

        // Add the forces
        var forces = _Selection.GetComponent<KObject>()._Forces;

        if (forces != null && forces.Count > 0)
        {
            foreach (var force in forces)
            {
                ForcesUI.AddNewForce(force.Magnitude, force.Angle);
            }
        }

        // hide the forces empty message
        ForceEmptyMessage.SetActive(false);
    }

    public void AddObjectTrajectory()
    {
        if (_Selection != null)
        {
            // Get forces
            var forces = _Selection.GetComponent<KObject>()._Forces;

            // Set the trajectory renderer position
            TrajectoryRenderer.transform.position = _Selection.transform.position;

            // If there is actually one or more forces acting on the object
            float xforce = 0; float yforce = 0;

            // Calculate the components of the resultant force
            foreach (var force in forces)
            {
                xforce += Mathf.Cos(Mathf.Deg2Rad * force.Angle) * force.Magnitude;
                yforce += Mathf.Sin(Mathf.Deg2Rad * force.Angle) * force.Magnitude;
            }

            // Calculate trajectory for every 0.05 seconds for 5 seconds
            Vector3[] poses = new Vector3[100];
            for (var t = 0; t < 100; t++)
            {
                var pos = GetPositionForTime(xforce, yforce, t / 20f);
                poses[t] = pos;
            }

            PositionTrajectoryTouchdown(poses);

            TrajectoryRenderer.SetPositions(poses);
            var distance = Vector3.Distance(TrajectoryRenderer.GetPosition(0), TrajectoryRenderer.GetPosition(99));
            TrajectoryRenderer.material.mainTextureScale = new Vector2(distance / 10, 1);
        }
    }

    /// <summary>
    /// Position the 
    /// </summary>
    /// <param name="poses"></param>
    private void PositionTrajectoryTouchdown(Vector3[] poses)
    {
        var near = 10000f;
        var nearestx = 0f;

        foreach (var pos in poses)
        {
            var actualPos = pos + _Selection.transform.position;

            if (near > Mathf.Abs(actualPos.y))
            {
                near = Mathf.Abs(actualPos.y);
                nearestx = actualPos.x;
            }
        }

        TouchdownObject.transform.position = new Vector3(nearestx, 0.01f, 0.5f);
    }

    private Vector3 GetPositionForTime(float x, float y, float t)
    {
        var currpos = _Selection.transform.position;

        // s = ut
        var posx = (float) (x * t);
        // s = ut - 1/2gtt
        var posy = (float) (y * t) + (0.5f * Physics.gravity.y * t * t);

        return new Vector3(posx, posy, 0);
    }

    private void UpdateTrajectoryTranslation()
    {
        TrajectoryRenderer.transform.position = _Selection.transform.position;
    }

    /// <summary>
    /// Trigger the property showcase from the selected item
    /// </summary>
    public void SelectionPropeties()
    {
        _ShowPropertiesDetails();
    }

    /// <summary>
    /// Show detailed properties in the properties panel based on the selected object
    /// </summary>
    private void _ShowPropertiesDetails ()
    {
        if (_Selection != null)
        {
            var rb = _Selection.GetComponent<Rigidbody>();
            // Set the name
            Name.text = _Selection.name;
            // Set the position and transform
            PosX.text = _Selection.transform.position.x.ToString();
            PosY.text = _Selection.transform.position.y.ToString();
            RotX.text = _Selection.transform.eulerAngles.x.ToString();
            RotY.text = _Selection.transform.eulerAngles.y.ToString();
            RotZ.text = _Selection.transform.eulerAngles.z.ToString();

            // Set the mass of the selected object
            Mass.text = _Selection.GetComponent<Rigidbody>().mass.ToString();

            // Get the type of material the object is using
            Material.value = Global._Helpers.matResolve(_Selection.GetComponent<KObject>()._Material); 
        }
        else
        {
            PropertyEmptyMessage.SetActive(true);
        }
    }

    /// <summary>
    /// Updates properties automatically on the global simulation periods
    /// </summary>
    public void UpdateProps()
    {
        if (Global.SimulationStarted)
        {
            _ShowPropertiesDetails();
        }
    }

    /// <summary>
    /// Delets the selected object
    /// </summary>
    public void DeleteSelection()
    {
        // Remove the gameObject from the editor
        DestroyImmediate(_Selection);
        // Nullify the selection
        _Selection = null;
        // Hide the object floater
        ObjectFloater.gameObject.SetActive(false);

        //TODO : Recompile the simulation
        _ShowPropertiesDetails();
    }

    public bool _pointer = false;
    private string _axis = "";
    private Vector2 _pointerPos = Vector3.zero;

    /// <summary>
    /// Called when the user clicks on a specific axis holder
    /// </summary>
    /// <param name="Axis"></param>
    public void TranslatePointerDown(string Axis)
    {
        if (_Selection != null)
        {
            _pointer = true;
            _axis = Axis;
            _pointerPos = Input.mousePosition;
        }
    }

    /// <summary>
    /// Called when the user releases a specific axis holder
    /// </summary>
    /// <param name="Axis"></param>
    public void TranslatePointerUp(string Axis)
    {
        _ShowPropertiesDetails();
        _pointer = false;
        _axis = "";
        _pointerPos = Vector3.zero;

        // saves the history
        _simulation.CreateSimulationObjectFromScene();
    }

    /// <summary>
    /// Move the selected object based on Flux Motion
    /// </summary>
    private void UpdateSelectionTranslation()
    {
        if (_pointer)
        {
            switch (_axis)
            {
                case "X":
                    var translationX = (Input.mousePosition.x - _pointerPos.x);
                    _Selection.transform.Translate(new Vector3(translationX / 5f * Time.deltaTime, 0, 0), Space.World);
                    break;
                case "Y":
                    var translationY = (Input.mousePosition.y - _pointerPos.y);
                    _Selection.transform.Translate(new Vector3(0, translationY / 5f * Time.deltaTime, 0), Space.World);
                    break;
            }

            UpdateFloaterPosition();
            UpdateTrajectoryTranslation();
            AddObjectTrajectory();
        }

        _pointerPos = Input.mousePosition;
    }

    /// <summary>
    /// Sets the values specified in the properties panel to the Selected object.
    /// </summary>
    public void SetPropertyValues()
    {
        // Set the name of the object
        if (!String.IsNullOrEmpty(Name.text))
            _Selection.name = Name.text;

        // Set the position of the object
        var x = _Selection.transform.position.x;
        var y = _Selection.transform.position.y;

        try
        {
            x = float.Parse(PosX.text);
            y = float.Parse(PosY.text);
        } catch (Exception) { }

        _Selection.transform.position = new Vector3(x, y, 0.5f);

        // Set the rotation of the object
        var rotx = _Selection.transform.rotation.x;
        var roty = _Selection.transform.rotation.y;
        var rotz = _Selection.transform.rotation.z;

        try
        {
            rotx = float.Parse(RotX.text);
            roty = float.Parse(RotY.text);
            rotz = float.Parse(RotZ.text);
        }
        catch (Exception) { }

        _Selection.transform.eulerAngles = new Vector3(rotx, roty, rotz);

        // Set the mass of the object
        var _sel_rb = _Selection.GetComponent<Rigidbody>();
        var mass = _sel_rb.mass;

        try
        {
            mass = float.Parse(Mass.text);
        }
        catch (Exception) { }

        _sel_rb.mass = mass;

        // Set the material of the object
        var comp = _Selection.GetComponent<KObject>();
        comp._Material = Global._Helpers.matResolveReverse(Material.value);
        comp.UpdatePhysicsMaterial();

        // Update the input fields back from the original values
        _ShowPropertiesDetails();
        UpdateFloaterPosition();
    }
}
