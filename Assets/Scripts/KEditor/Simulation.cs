﻿
using System;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace KEditor
{
    /// <summary>
    /// Manages everything about the current simulation
    /// </summary>
    public class Simulation : MonoBehaviour
    {
        public global::Simulation _CurrentSimulation;

        [Header("Live GameObject Connections")]
        public Text _SimulationTimer;
        public GameObject RunButton, StopButton;
        public Image SimulationStatusBulb;
        public Text SimulationStatusText;
        public Text UpperSimulationName;
        public Text SimulationNameOnSimulationPanel;
        public Dropdown GravityScaleInput;
        public Switch GravityEnabled;

        [Header("Simulation Renamer")]
        public GameObject SimulationRenamer;
        public InputField SimulationRenamerInput;

        [Header("KObjects")]
        public GameObject Baseball;
        public GameObject Tennisball;
        public GameObject Basketball;
        public GameObject Football;
        public GameObject InclinedPath;
        public GameObject _45DegPath;
        public GameObject WoodBlock;

        // Private fields
        private float _simulationTimer = 0f;

        void Awake()
        {
            // Set the simulation object
            History._CurrentSimulation = this;
            // Get the simulation object from the global parser
            // Used when you're loading an existing simulation or something
            _CurrentSimulation = Global.SimulationObject;
            // Stop the physics simulation 
            _SetSimulation(false);
            // Create scene from simulation object
            CreateSceneFromSimulationObject();

            // Add the first simulation history
            SaveHistory();

            /*
             *  SAVE THIS PROJECT AS A RECENT PROJECT ON PLAYERPREFS
             *  
             */
        }

        public const float GRAVITY_OF_EARTH = -9.80665f;
        public float[] gravityScales = new float[] { 1f, 0.16f, 0.38f, 0.9f, 2.54f, 28f };

        private float _tempGravity = GRAVITY_OF_EARTH;

        /// <summary>
        /// Sets new gravity scales
        /// </summary>
        public void SetGravityScale()
        {
            // Only if gravity is enabled
            if (GravityEnabled.Selected == Switch.Selection.Left)
            {
                var new_gravity = GRAVITY_OF_EARTH * gravityScales[GravityScaleInput.value];
                Physics.gravity = new Vector3(0, new_gravity, 0);

                // Save the new gravity scale
                CreateSimulationObjectFromScene();
            }

            GetComponent<Selector>().AddObjectTrajectory();
        }

        /// <summary>
        /// Toggles gravity based on the gravity enabled input
        /// </summary>
        public void ToggleGravity()
        {
            if (GravityEnabled.Selected == Switch.Selection.Right)
            {
                // Temporarily disable gravity
                _tempGravity = Physics.gravity.y;
                // Disable the gravity
                Physics.gravity = Vector3.zero;
            }
            else
            {
                // Enable gravity
                Physics.gravity = new Vector3(0, _tempGravity, 0);
            }

            GetComponent<Selector>().AddObjectTrajectory();
        }

        /// <summary>
        /// Shares the simulation with your friends
        /// </summary>
        public void ShareSimulation()
        {
            Share.ShareText("Hey, checkout my physics simulation", "http://sim.kinetix.com/" + _CurrentSimulation.id);
        }

        public void Undo()
        {
            // Undo the history
            History.Undo();
        }

        private void SaveHistory()
        {
            History._History.Push(_CurrentSimulation);
        }

        void Update()
        {
            if (Global.SimulationStarted && Physics.autoSimulation == true)
            {
                // Calculate the timer  
                _simulationTimer += Time.deltaTime;

                var mills = Mathf.Round(_simulationTimer * 100) / 100f * 1000;
                var time = TimeSpan.FromMilliseconds(mills);

                // Show the timer
                _SimulationTimer.text = string.Format("{0:D2}:{1:D2}.{2:D3}", time.Minutes, time.Seconds, time.Milliseconds);
            }

            SimulationNameOnSimulationPanel.text = UpperSimulationName.text = _CurrentSimulation.name;

            if (OnRenameSimulation && Input.GetKeyDown(KeyCode.Escape))
            {
                // Close the rename window
                CloseRename();
            }
        }

        private bool OnRenameSimulation = false;

        /// <summary>
        /// Closes the rename simulation window without saving the changes
        /// </summary>
        private void CloseRename()
        {
            SimulationRenamer.SetActive(false);
        }

        /// <summary>
        /// Renames the simulation based on the given input
        /// </summary>
        public void RenameSimulation()
        {
            // Rename the simulation name and hide the renamer input
            _CurrentSimulation.name = SimulationRenamerInput.text;
            SimulationRenamer.SetActive(false);

            SaveHistory();
        }

        /// <summary>
        /// Shows the Rename input of the simulation
        /// </summary>
        public void Rename()
        {
            // Show the simulation renamer
            SimulationRenamer.SetActive(true);
            SimulationRenamerInput.text = _CurrentSimulation.name;
        }

        void ResetTimer()
        {
            // Reset the simulation timer
            _simulationTimer = 0f;
            _SimulationTimer.text = "00:00.000";
        }

        public void CreateObject(string objname)
        {
            // Create Object
            var obj = ResolveObject(objname);
            // Spawn the object on a random position
            var spawned = Instantiate(obj, new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, 0.5f), Quaternion.Euler(new Vector3(0, 0, 0)));
            // Select the spawned object
            Global._Selector.Select(spawned);

            // Hide the objects menu
            GetComponent<Toggler>().Toggle("Objects");

            // Generate the simulation object
            CreateSimulationObjectFromScene();
        }

        /// <summary>
        /// Resolve the object based on its identifier name
        /// </summary>
        /// <param name="objname"></param>
        /// <returns></returns>
        private GameObject ResolveObject(string objname)
        {
            switch (objname)
            {
                case "Baseball":
                    return Baseball;
                case "Tennisball":
                    return Tennisball;
                case "Basketball":
                    return Basketball;
                case "Football":
                    return Football;
                case "InclinedPath":
                    return InclinedPath;
                case "45degPath":
                    return _45DegPath;
                case "WoodBlock":
                    return WoodBlock;
                default:
                    return Baseball;
            }
        }

        /// <summary>
        /// Puts objects, sets their properties according to the simulation object
        /// </summary>
        /// <returns></returns>
        public bool CreateSceneFromSimulationObject()
        {
            // Destroy all the existing objects
            var kobjects = GameObject.FindGameObjectsWithTag("Objects");
                
            foreach (var obj in kobjects)
            {
                Destroy(obj);
            }

            // Spawn the objects back
            var elements = _CurrentSimulation.elements;

            if (elements != null & elements.Count > 0)
            {
                foreach (var elem in elements)
                {
                    GameObject spawn = ResolveObject(elem.type);

                    // Spawn the element
                    var spawned = Instantiate(spawn, new Vector3(elem.position.X, elem.position.Y, 0.5f), Quaternion.Euler(new Vector3(elem.rotation.X, elem.rotation.Y, elem.rotation.Z)));
                    // Add forces to the spawned object
                    var comp = spawned.GetComponent<KObject>();
                    comp._Forces = elem.forces;
                    comp._Material = Global._Helpers.matResolveReverse(int.Parse(elem.material.ToString()));
                    comp.UpdatePhysicsMaterial();
                }
            }

            GetComponent<Selector>().Deselect();

            return true;
        }

        IEnumerator SetTimeout(int seconds, Action action)
        {
            yield return new WaitForSeconds(seconds);
            action();
        }

        /// <summary>
        /// Creates a simulation object based on the game objects on the scene nad their properties
        /// </summary>
        /// <returns></returns>
        public bool CreateSimulationObjectFromScene()
        {
            // Get the KinetiX Physics based Game Objects
            var kobjects = GameObject.FindGameObjectsWithTag("Objects");

            List<Element> elements = new List<Element>();

            foreach (var obj in kobjects)
            {
                var comp = obj.GetComponent<KObject>();
                var rb = obj.GetComponent<Rigidbody>();

                elements.Add(new Element(obj.name, comp.Type, 
                    new Vector(obj.transform.position.x, obj.transform.position.y, obj.transform.position.z)
                    , new Vector(obj.transform.eulerAngles.x, obj.transform.eulerAngles.y, obj.transform.eulerAngles.z)
                    , rb.mass, Global._Helpers.matResolve(comp._Material), 0, comp._Forces));
            }

            // Update the current simulation from the generated object
            _CurrentSimulation = new global::Simulation(_CurrentSimulation.name, _CurrentSimulation.id, elements, Physics.gravity.y, _CurrentSimulation.gravityEnabled, _CurrentSimulation.localFile);

            Save();

            SaveHistory();

            return true;
        }
        
        /// <summary>
        /// Call the cloud save commands to save the simulations on the servers
        /// </summary>
        public void Save()
        {
            // Save to the cloud
            CloudSave.Save(_CurrentSimulation);
        }

        /// <summary>
        /// Starts the simulation
        /// </summary>
        public void Run()
        {
            CreateSimulationObjectFromScene();
            _SetSimulation(true);

            var kobjects = GameObject.FindGameObjectsWithTag("Objects");

            foreach (var obj in kobjects)
            {
                var comp = obj.GetComponent<KObject>();
                var rb = obj.GetComponent<Rigidbody>();

                foreach (var force in comp._Forces)
                {
                    rb.AddForce(new Vector3(force.Magnitude * 10f * Mathf.Cos(Mathf.Deg2Rad * force.Angle), force.Magnitude * -Physics.gravity.y * Mathf.Sin(Mathf.Deg2Rad * force.Angle), 0),ForceMode.Force);
                }
            }

            // Disable the run button
            RunButton.SetActive(false);
            StopButton.SetActive(true);

            // Set the simulation status
            SimulationStatusBulb.color = Color.red;
            SimulationStatusText.text = "Simulation Started";
        }

        /// <summary>
        /// Restart the simulation based on the cached document
        /// </summary>
        public void Restart()
        {
            // Respawn the objects in their original positions
            CreateSceneFromSimulationObject();
            // Stop the physics simulation
            _SetSimulation(false);
            // Reset the simulation timer
            ResetTimer();

            // Enable the run button
            RunButton.SetActive(true);
            StopButton.SetActive(false);

            // Set the simulation status
            SimulationStatusBulb.color = Color.red;
            SimulationStatusText.text = "Simulation Paused";
        }

        /// <summary>
        /// Stops the simulation entirely
        /// </summary>
        public void Stop()
        {
            // Stop the simulation
            _SetSimulation(false);
            // Reset the objects
            CreateSceneFromSimulationObject();
            // Reset the timer
            ResetTimer();

            // Hide the simulation timer
            GetComponent<Toggler>().Toggle("Simulation");

            // Enable the run button
            RunButton.SetActive(true);
            StopButton.SetActive(false);

            // Set the simulation status
            SimulationStatusBulb.color = new Color(150, 150, 150);
            SimulationStatusText.text = "Edit Mode";
        }

        /// <summary>
        /// Pauses the physics simulation
        /// </summary>
        public void Pause()
        {
            Physics.autoSimulation = false;

            // Enable the run button
            RunButton.SetActive(true);
            StopButton.SetActive(false);

            // Set the simulation status
            SimulationStatusBulb.color = Color.red;
            SimulationStatusText.text = "Simulation Paused";
        }

        /// <summary>
        /// Set Simulation status
        /// </summary>
        /// <param name="state">State of the simulation, true for auto simulation and false for paused simulation</param>
        private void _SetSimulation(bool state)
        {
            Global.SimulationStarted = state;
            Physics.autoSimulation = state;
        }

    }
}