﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Represents a single object in the KinetiX Editor.
/// </summary>
public class KObject : MonoBehaviour
{
    public enum Material
    {
        None, FullFriction, Bouncy, Bouncy_Friction
    }
    
    public List<Force> _Forces = new List<Force>();
    public string Type = "";
    public Material _Material = Material.None;

    private Rigidbody _rb;
    private Collider _coll;

    public void Awake()
    {
        _rb = GetComponent<Rigidbody>();
        _coll = GetComponent<Collider>();
    }
            
    /// <summary>
    /// Update the physics material of the object based on the given properties
    /// </summary>
    public void UpdatePhysicsMaterial()
    {
        switch (_Material)
        {
            case Material.None:
                _rb.drag = 0f; // No air resistance on linear translation
                _rb.angularDrag = 0.05f; // No air resistance on angular translation
                _coll.material.bounciness = 0; // No bounce (100% energy loss)
                // No friction at all (Smooth surfacing)
                _coll.material.dynamicFriction = 0;
                _coll.material.staticFriction = 0;
                break;
            case Material.FullFriction:
                _rb.angularDrag = 0.5f; // Bit angular resistance
                _rb.drag = 0f; // No air resistance
                _coll.material.bounciness = 0; // Still no bounciness
                _coll.material.dynamicFriction = 1;
                _coll.material.staticFriction = 1;
                break;
            case Material.Bouncy:
                _rb.angularDrag = 0.02f; // No air resistance on angualr translation
                _rb.drag = 0f; // No air resistance on linear translation
                _coll.material.bounciness = 0.8f; // Bounciness with 20% energy loss
                // No Friction (smooth surface)
                _coll.material.dynamicFriction = 0f;
                _coll.material.staticFriction = 0f;
                break;
            case Material.Bouncy_Friction:
                _rb.angularDrag = 0.5f; // A bit angular resistance
                _rb.drag = 0; // No air resistance
                _coll.material.bounciness = 0.8f; // Bounciness with 20% energy loss
                _coll.material.dynamicFriction = 0.4f; // Bit dynamic friction
                _coll.material.staticFriction = 0.4f;
                break;
        }
    }
}
