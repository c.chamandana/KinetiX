﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Firebase.Database;

public static class Global
{
    public static Helpers _Helpers;
    public static bool SimulationStarted = false;
    public static Forces _ForcesPanel = null;
    public static Simulation SimulationObject = new Simulation("Untitled Simulation", (UnityEngine.Random.Range(0, 10000000)).ToString(), new List<Element>(), -9.80665f, true, "");
    public static Selector _Selector = null;
    public static DatabaseReference _Firebase = null;
    public static bool GraphViewEnabled = false;
}