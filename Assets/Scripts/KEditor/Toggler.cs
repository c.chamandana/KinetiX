﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class Toggler : MonoBehaviour
{
    [Header("Panels")]
    public RectTransform Objects;
    public RectTransform Properties;
    public RectTransform Forces;
    public RectTransform Simulation;

    [Header("Toggling Properties")]
    public float AnimationScale = 5f;

    /// <summary>
    /// Toggles the specified panel
    /// </summary>
    /// <param name="Panel">Panel to toggle</param>
    public void Toggle(string Panel)
    {
        RectTransform selectedPanel = null;

        switch (Panel)
        {
            case "Objects":
                selectedPanel = Objects;
                break;
            case "Properties":
                selectedPanel = Properties;
                break;
            case "Forces":
                selectedPanel = Forces;
                break;
            case "Simulation":
                selectedPanel = Simulation;
                break;
        }

        // Show the panel first
        selectedPanel.gameObject.SetActive(true);
        var panelComp = selectedPanel.GetComponent<Panel>();
        UntoggleExcept(selectedPanel);
        StartCoroutine(TogglePanel(selectedPanel, panelComp._Direction, panelComp._State, panelComp._ExpandedWidth));
    }

    void Start()
    {
        // Hide all the panels until they are called
        Objects.gameObject.SetActive(false);
        Properties.gameObject.SetActive(false);
        Forces.gameObject.SetActive(false);
        Simulation.gameObject.SetActive(false);
    }

    /// <summary>
    /// Untoggles every panel except the selected one
    /// </summary>
    /// <param name="panel">The panel to stay toggled</param>
    public void UntoggleExcept(RectTransform panel)
    {
        RectTransform[] panels = { Objects, Properties, Forces, Simulation };
        RectTransform[] panelRem = { panel };
        var untoggle = panels.Except(panelRem);

        foreach (var p in untoggle)
        {
            var comp = p.GetComponent<Panel>();

            if (comp._State == Panel.PanelState.Expanded)
            {
                StartCoroutine(TogglePanel(p, comp._Direction, comp._State, comp._ExpandedWidth));
            }
        }
    }

    /// <summary>
    /// Animates the toggling effect of the panels
    /// </summary>
    /// <param name="panel"></param>
    /// <returns></returns>
    public IEnumerator TogglePanel(RectTransform panel, Panel.Direction direction, Panel.PanelState state, float targetWidth)
    {
        if (direction == Panel.Direction.Horizontal)
        {
            if (state == Panel.PanelState.Collapsed)
            {
                // Panel is collapsed
                while (panel.anchoredPosition.x < -2)
                {
                    panel.anchoredPosition = new Vector2(Mathf.Lerp(panel.anchoredPosition.x, -2, Time.deltaTime * AnimationScale), panel.anchoredPosition.y);

                    if (panel.anchoredPosition.x > -3f)
                    {
                        panel.gameObject.GetComponent<Panel>()._State = Panel.PanelState.Expanded;
                        break;
                    }

                    yield return null;
                }
            }
            else
            {
                // Panel is expanded
                while (panel.anchoredPosition.x > -targetWidth)
                {
                    panel.anchoredPosition = new Vector2(Mathf.Lerp(panel.anchoredPosition.x, -targetWidth - 2, Time.deltaTime * AnimationScale), panel.anchoredPosition.y);

                    if (panel.anchoredPosition.x < -targetWidth)
                    {
                        panel.gameObject.GetComponent<Panel>()._State = Panel.PanelState.Collapsed;
                        panel.gameObject.SetActive(false);
                        break;
                    }

                    yield return null;
                }
            }
        }
        else
        {
            if (state == Panel.PanelState.Collapsed)
            {
                // Panel is collapsed
                while (panel.anchoredPosition.y < 0)
                {
                    panel.anchoredPosition = new Vector2(panel.anchoredPosition.x, Mathf.Lerp(panel.anchoredPosition.y, 0, Time.deltaTime * AnimationScale));

                    if (panel.anchoredPosition.y > -1f)
                    {
                        panel.gameObject.GetComponent<Panel>()._State = Panel.PanelState.Expanded;
                        break;
                    }

                    yield return null;
                }
            }
            else
            {
                // Panel is expanded
                while (panel.anchoredPosition.y > -targetWidth)
                {
                    panel.anchoredPosition = new Vector2(panel.anchoredPosition.x, Mathf.Lerp(panel.anchoredPosition.y, -targetWidth - 2, Time.deltaTime * AnimationScale));

                    if (panel.anchoredPosition.y < -targetWidth)
                    {
                        panel.gameObject.GetComponent<Panel>()._State = Panel.PanelState.Collapsed;
                    }

                    yield return null;
                }
            }
        }
    }
}
