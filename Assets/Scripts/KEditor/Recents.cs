﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class Recents
{
    /// <summary>
    /// Get the recent simulations the editor has opened
    /// </summary>
    /// <returns></returns>
    public static List<Recent> GetRecents()
    {
        var value = PlayerPrefs.GetString("recents");
        List<Recent> RecentSimulations = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Recent>>(value);
        return RecentSimulations;
    }

    public static void AddRecent(Simulation _sim)
    {
        List<Recent> current = new List<Recent>();

        // If the list is not empty
        if (!String.IsNullOrEmpty(PlayerPrefs.GetString("recents")))
        {
            current = GetRecents();
        }

        // Add the current simulation
        current.Add(new Recent(_sim.name, _sim.id));

        // Save the player preferences
        PlayerPrefs.SetString("recents", Newtonsoft.Json.JsonConvert.SerializeObject(current));

        PlayerPrefs.Save();
    }

    /// <summary>
    /// Model of a recently opened simulation
    /// </summary>
    [Serializable]
    public class Recent
    {
        public string SimulationName;
        public string SimulationID;

        public Recent(string simName, string simID)
        {
            SimulationName = simName;
            SimulationID = simID;
        }
    }

}
