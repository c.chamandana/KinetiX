﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectInformation : MonoBehaviour
{
    public GameObject InformationDisplay;

    [Header("Information Display fields")]
    public Text Velocity;
    public Text MaximumVelocity;
    public Text Displacement;
    public Text ObjectName;

    private Vector2 _tempMaxVel = Vector2.zero;

    void Update()
    {
        if (Global._Selector._Selection != null && Global.SimulationStarted)
        {
            // An object is selected
            var rb = Global._Selector._selectionrb;
            Velocity.text = "(" + (System.Math.Round(rb.velocity.x,2)).ToString() + " , " + (System.Math.Round(rb.velocity.y)).ToString() + ")";

            // Calculate what's the maximum velocity is
            if (Mathf.Abs(rb.velocity.x) > _tempMaxVel.x) _tempMaxVel.x = (float) System.Math.Round(Mathf.Abs(rb.velocity.x), 2);
            if (Mathf.Abs(rb.velocity.y) > _tempMaxVel.y) _tempMaxVel.y = (float)System.Math.Round(Mathf.Abs(rb.velocity.y), 2);

            MaximumVelocity.text = "(" + _tempMaxVel.x.ToString() + " , " + _tempMaxVel.y.ToString() + ")";

            // Get the displacement
            var distance = Vector3.Distance(Global._Selector._selectionOriginalPosition, Global._Selector._Selection.transform.position);
            var displacement = Global._Selector._Selection.transform.position - Global._Selector._selectionOriginalPosition;


            // Show the displacement and the distance the object has travelled
            Displacement.text = System.Math.Round(distance, 2).ToString() + "(" + System.Math.Round(displacement.x, 2).ToString() + " , " + System.Math.Round(displacement.y, 2).ToString() + ")";

            ObjectName.text = Global._Selector._Selection.name;
        }
        else
        {
            Velocity.text = "NaN";
            MaximumVelocity.text = "NaN";
            Displacement.text = "NaN";
            ObjectName.text = "No Object Selected";

            _tempMaxVel = Vector2.zero;
        }
    }

    /// <summary>
    /// Toggle the information display panel
    /// </summary>
    public void ToggleInformationDisplay()
    {
        InformationDisplay.SetActive(!InformationDisplay.activeInHierarchy);
    }
}
