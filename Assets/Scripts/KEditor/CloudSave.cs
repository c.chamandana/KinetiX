﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public static class CloudSave
{ 
    public static Task Save(Simulation _sim)
    {
#if (UNITY_ANDROID)
        return Global._Firebase.Child("sims").Child(_sim.id).SetRawJsonValueAsync(JsonUtility.ToJson(_sim));
#endif
        return null;
    }

    public static Simulation Load(string id)
    {
        return new Simulation("Untitled Simulation", id, new List<Element>(), -9.80665f, true, id + ".sim");
    }
}
