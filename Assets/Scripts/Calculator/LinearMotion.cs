﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LinearMotion : MonoBehaviour, ICalculatable
{
    [Header("v = u + at")]
    public InputField InitialVelocity_1;
    public InputField Acceleration_1;
    public InputField TimeOfMotion_1;
    public Text FinalVelocity_1;

    [Header("s = ut + 1/2 att")]
    public InputField InitialVelocity_2;
    public InputField Acceleration_2;
    public InputField TimeOfMotion_2;
    public Text Displacement_2;

    [Header("v2 = u2 + 2as")]
    public InputField InitialVelocity_3;
    public InputField Acceleration_3;
    public InputField Displacement_3;
    public Text FinalVelocity_3;

    [Header("s = (u + v) / 2 * t")]
    public InputField InitialVelocity_4;
    public InputField FinalVelocity_4;
    public InputField TimeOfMotion_4;
    public Text Displacement_4;

    public void Change()
    {
        try
        {
            // v = u + at
            FinalVelocity_1.text = ((float.Parse(InitialVelocity_1.text)) + (float.Parse(Acceleration_1.text)) * (float.Parse(TimeOfMotion_1.text))).ToString();
            // s = ut + 1/2 att
            Displacement_2.text = ((float.Parse(InitialVelocity_2.text)) * (float.Parse(TimeOfMotion_2.text)) + (0.5f * (float.Parse(Acceleration_2.text)) * (float.Parse(TimeOfMotion_2.text)) * (float.Parse(TimeOfMotion_2.text)))).ToString();
            // v2 = u2 + 2as
            FinalVelocity_3.text = Mathf.Sqrt(float.Parse(InitialVelocity_3.text) * float.Parse(InitialVelocity_3.text) + 2 * float.Parse(Acceleration_3.text) * float.Parse(Displacement_3.text)).ToString();
            // s = (u + v) / 2 * t
            FinalVelocity_3.text = ((float.Parse(InitialVelocity_4.text) + float.Parse(FinalVelocity_4.text)) * float.Parse(TimeOfMotion_4.text) / 2).ToString();
        }
        catch (Exception) { }
    }
}
