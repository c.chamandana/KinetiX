﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GravitationalEquation : MonoBehaviour, ICalculatable
{
    public InputField Mass1;
    public InputField Mass2;
    public InputField Distance;
    public Text Force;

    public void Change()
    {
        try
        {
            Force.text = (float.Parse(Mass1.text) * float.Parse(Mass2.text) * LocalGravity.G / (float.Parse(Distance.text) * float.Parse(Distance.text))).ToString();
        }
        catch (Exception) { }
    }
}
