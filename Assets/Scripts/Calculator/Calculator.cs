﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public interface ICalculatable
{
    void Change();
}

public class Calculator : MonoBehaviour
{
    [Header("GameObject Connections")]
    public GameObject LinearMotion;
    public GameObject GravitationalEquation;

    /// <summary>
    /// Resolve the tab based on its identifier
    /// </summary>
    /// <param name="tabname"></param>
    /// <returns></returns>
    private GameObject ResolveTab(string tabname)
    {
        switch (tabname)
        {
            case "LinearMotion":
                return LinearMotion;
            case "GravitationalEquation":
                return GravitationalEquation;
            default:
                return null;
        }
    }

    /// <summary>
    /// Hide all the available tabs
    /// </summary>
    private void HideAllTabs()
    {
        HideTab(new GameObject[] 
        {
            LinearMotion, GravitationalEquation 
        });
    }

    /// <summary>
    /// Hide a specified array of tabs
    /// </summary>
    /// <param name="tabs"></param>
    private void HideTab(GameObject[] tabs)
    {
        foreach (var tab in tabs)
            tab.SetActive(false);
    }

    /// <summary>
    /// Switch to the 
    /// </summary>
    /// <param name="tab"></param>
    public void Switch(Button btn)
    {   
        // Hide all the tabs
        HideAllTabs();

        // Disable the backgrounds of all the buttons
        var switchers = GameObject.FindGameObjectsWithTag("TabSwitcher");
        foreach (var switcher in switchers)
        {
            switcher.GetComponent<Image>().color = new Color(1, 1, 1, 0);
        }

        // Colorize the background of this button
        btn.GetComponent<Image>().color = new Color(1, 1, 1, 14f / 255f);

        // Show only the given tab
        var _tab = ResolveTab(btn.GetComponent<Calculatable>().Identifier);
        _tab.SetActive(true);
    }
}
