﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class CameraMotion : MonoBehaviour
{
    // The bounds for the camera pannning
    [Header("Camera Pan Bounds")]
    public Vector2 Minimum;
    public Vector2 Maximum;

    private Vector3 previousPosition = Vector3.zero;
    private bool _mouseDowned = false;

    void Update()
    {
        if (!Global._Selector._pointer)
        {
            if (Input.GetMouseButtonDown(0) && Global._Helpers.CheckBounds(Input.mousePosition))
            {
                _mouseDowned = true;
            }

            if (Input.GetMouseButtonUp(0))
            {
                _mouseDowned = false;
                // Reset the previous position
                previousPosition = Vector3.zero;
            }

            if (_mouseDowned)
            {
                if (previousPosition != Vector3.zero)
                {
                    // Get the difference in mouse position
                    var diff = (Input.mousePosition - previousPosition);

                    // Position the camera
                    var translation = new Vector3(-diff.x * Time.deltaTime / 10f, -diff.y * Time.deltaTime / 10f, 0);
                    transform.Translate(translation);

                    transform.position = new Vector3(Mathf.Clamp(transform.position.x, Minimum.x, Maximum.x), Mathf.Clamp(transform.position.y, Minimum.y, Maximum.y), transform.position.z);
                }

                // Set the previous position
                previousPosition = Input.mousePosition;
            }
        }
    }
}