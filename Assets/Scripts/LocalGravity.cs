﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalGravity : MonoBehaviour
{
    public const float G = 6.67e-5f;
    public Transform target;

    Rigidbody target_rb;
    Rigidbody rb;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        target_rb = target.gameObject.GetComponent<Rigidbody>();
    }
    
    // Update is called once per frame
    void Update()
    {
        var direction = target_rb.position - rb.position;
        var distance = Vector3.Distance(target_rb.position, rb.position);

        rb.AddForce(direction * (G * Time.deltaTime * rb.mass * target_rb.mass / (distance * distance)));

        transform.RotateAround(target.position, Vector3.up, target_rb.mass / 6.67e11f * Time.deltaTime);
    }
}
